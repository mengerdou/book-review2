# Planning

make a virtual env
activate virtual env
install django
create django project
add a django app to the project
start django server
make some templates
* [ ] `python manage.py startapp reviews`
* [ ] make migrations
  * [ ] `python manage.py makemigrations`
* [ ] migrate
  * [ ] `python manage.py migrate`
* [ ] Start Django server
  * [ ] `python manage.py runserver`
* [ ] Make some templates